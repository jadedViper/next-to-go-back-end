# README #

This repository is the back end solution to the Neds Technical Test

### What is this repository for? ###

* Neds Technical Test (Back end)
* Version 1
* Written with PHP
* Backend proxy to the Tabcorp api

### How do I get set up? ###

* pull repository to local server
* run composer install to install dependencies
* Has one function proxiendpoint.atyourserver?action=getRaces&jurisdiction=<Jurisdiction>
  * parameter jurisdiction is required and can be one of NSW, VIC or ACT
* This backend is designed as a proxy for the Neds test front end located at:
  * [https://bitbucket.org/jadedViper/next-to-go-front-end/overview](https://bitbucket.org/jadedViper/next-to-go-front-end/overview)