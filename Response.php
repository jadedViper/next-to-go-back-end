<?php

namespace Response;
require_once __DIR__.'/vendor/autoload.php';

class Response {
    private $response;

    public function __construct($response = null) {
        $this->response = $response;
    }

    public function setBody($response) {
        $this->response = $response;
    }

    public function setHeaders($statusCode, $contentLength) {
        http_response_code($statusCode);
        header('Content-Type: application/json');
        header('Content-Length: '.$contentLength);
        header('Access-Control-Allow-Origin: *');
    }

    public function json() {
        if (!is_array($this->response) && !is_object($this->response)) {
            throw new \Exception('Invalid response data');
        }
        $content = json_encode($this->response);
        $this->setHeaders(200, strlen($content));
        print $content;
    }
}
