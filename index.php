<?php
require_once __DIR__.'/APIProxy.php';
require_once __DIR__.'/Response.php';

use APIProxy\APIProxy;
use Response\Response;

try {
    $proxy = new APIProxy();

    if (!isset($_GET['action'])) {
        throw new Exception('Action not available');
    }

    $method = 'action'.str_replace(' ', '', ucwords($_GET['action']));
    if (!method_exists($proxy, $method)) {
        throw new Exception('No such service');
    }

    $content = $proxy->${'method'}($_GET);
    $response = ['data' => $content, 'result' => true];
} catch (Exception $exception) {
    $response = ['data' => $exception->getMessage(), 'result' => false];
}

$httpResponse = new Response($response);
$httpResponse->json();
