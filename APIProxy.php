<?php

namespace APIProxy;

require_once __DIR__.'/vendor/autoload.php';
use GuzzleHttp\Client;

class APIProxy {
    const URL = 'https://api.beta.tab.com.au/v1/tab-info-service/racing/next-to-go/';
    protected $client;

    public function __construct() {
        $this->client = new Client(['base_uri' => self::URL]);
    }

    public function actionGetRaces($params) {
        if (empty($params['jurisdiction'])) {
            throw new \Exception('Missing jurisdiction for race request');
        }
        $response = $this->client->request('GET', 'races?jurisdiction='.$params['jurisdiction']);
        if ($response->getStatusCode() !== 200) {
            throw new \Exception($response->getReasonPhrase());
        }
        return json_decode($response->getBody(), true);
    }
}
